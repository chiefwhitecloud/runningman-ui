RUNNER_LESS = ./css/less/runner.less
DATE=$(shell date +%I:%M%p)
CHECK=\033[32m✔\033[39m
HR=\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#

prod:
	mkdir -p htdocs/img
	mkdir -p htdocs/css
	mkdir -p htdocs/js
	cp ./css/bootstrap/img/* htdocs/img/
	cp img/*.png htdocs/img/
	cp img/*.jpg htdocs/img/
	cp img/*.gif htdocs/img/
	cp js/*.js htdocs/js/
	cp *.html htdocs/
	cp css/*.* htdocs/css/
	recess --compile ${RUNNER_LESS} > htdocs/css/styles.css
	recess --compress ${RUNNER_LESS} > htdocs/css/styles.min.css
	

