
var Events = function(){

}

/* static getAll events*/
/* returns Array of Event objects */
Events.getAll = function(opt_year){
	var request = new Request();
	return request.Process('events', function(item){ return new Event(item) });
}

var Event = function(obj){
	this.id = obj.event_id;
	this.name = obj.name;
}

Event.prototype.getRaces = function(){
	var request = new Request();
	return request.Process('event' + '/' + this.id + '/races', function(item){ return new Race(item) });
}

var Runner = function(){

}

Runner.prototype.getRaces = function(){
}

Runner.prototype.getResults = function(){
}

var Races = function(){

}

/* static getAll races*/
/* returns Array of Race objects */
Races.getAll = function(opt_year){
	var request = new Request();
	return request.Process('races', function(item){ return new Race(item) });
}

var Race = function(obj){
	this.id = obj.race_id;
	this.location = obj.location;
	this.eventname = obj.eventname;
	this.date = new Date(obj.year, (obj.month - 1), obj.day);
}

Race.prototype.getResults = function(){
	var request = new Request();
	return request.Process('race' + '/' + this.id + '/results', function(item){ return new Result(item) });
}

var Result = function(obj){
	this.firstname = obj.runner.firstname;
	this.lastname = obj.runner.lastname;
	this.sex = obj.runner.sex;
	this.bib = obj.bibnumber;
	this.hours = obj.hours;
	this.minutes = obj.minutes;
	this.seconds = obj.seconds;
	this.overallPlace = obj.overallplace;
	this.sexPlace = obj.sexplace;
	this.pace = Helper.formatTime(0, obj.paceminutes, obj.paceseconds);
}

var Helper = {}

Helper.formatTime = function(h, m, s){
	
	var sep = ":",
		 ret =  '',
		 hasTimeBeenFound = false,
		 el  = [h,m,s];

	_.each(el, function(el){
		if (el && el !== 0){
			hasTimeBeenFound = true;
		}

		if (hasTimeBeenFound){
			
			if (el.toString() == '0' || el.toString().length == 1){
				ret += '0' + el.toString();
			}
			else if (el){
				ret += el.toString();
			}
			ret += ':';
		}
	});

	return ret.substring(0, ret.length -1);
}


var Defer = function(){

}

Defer.prototype.addCallback = function(fn){
	this.callback = fn;
	return this;
}

Defer.prototype.addErrback = function(fn){
	this.errback = fn;
	return this;
}


var Request = function(){
	this._base = '/api/'
}

Request.prototype.Process = function(url, fn){
	var req = this.Send(url);
	var def = new Defer();
	req.success(function(response){
		var ret = [];
			_.each(response, function(item) {
				var result = fn.call(this, item);			
				ret.push(result);
  			});
			def.callback.call(this, ret);
		});

		req.fail(function(){
			def.errback.call();
		});

		return def;
}

Request.prototype.Send = function(location){
	return $.getJSON(this._base + location);
}



