
window.Runner = Backbone.Model.extend();
window.Race = Backbone.Model.extend();
window.RunnerResult = Backbone.Model.extend();
window.RaceResult = Backbone.Model.extend();

window.RunnerCollection = Backbone.Collection.extend({
    model: Runner,
    url:"../api/runners"
});

window.RaceCollection = Backbone.Collection.extend({
	 initialize: function(models, options) {
		if (options)
    		this.id = options.id;
  	 },
    model: Race,
	 url: function() {
      return this.id ? "../api/event/" + this.id + "/races" : "../api/races";
    },
	 comparator: function(race){
		var date  = new Date(race.get("year"), (race.get("month") - 1), race.get("day"));
      return -date.getTime();
	 }
});

window.RunnerResultsCollection = Backbone.Collection.extend({
	 initialize: function(models, options) {
    	this.id = options.id;
  	 },
    model: RunnerResult,
	 url: function() {
      return '../api/runner/' + this.id + '/results';
    },
 	comparator: function(result){
		var date  = new Date(result.get("race").year, (result.get("race").month - 1), result.get("race").day);
      return -date.getTime();
	}
});

window.RaceResultsCollection = Backbone.Collection.extend({
	 initialize: function(models, options) {
    	this.id = options.id;
  	 },
    model: RaceResult,
	 url: function() {
      return '../api/race/' + this.id + '/results';
    }
});
 
// Views
window.RunnerListView = Backbone.View.extend({
 
    tagName:'ul',
 
    initialize:function () {
        this.model.bind("reset", this.render, this);
    },
 
    render:function (eventName) {
        _.each(this.model.models, function (runner) {
            $(this.el).append(new RunnerListItemView({model:runner}).render().el);
        }, this);
        return this;
    }
 
});
 
window.RunnerListItemView = Backbone.View.extend({
 
    tagName:"li",
 
    template:_.template($('#tpl-runner-list-item').html()),
 
    render:function (eventName) {
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }
 
});


// Views
window.RaceListView = Backbone.View.extend({
 
    tagName:'div',
 
    initialize:function () {
        this.model.bind("reset", this.render, this);
    },
 
    render:function (eventName) {
			var year = null;
        _.each(this.model.models, function (race) {
			
				if (race.get("year") != year){
					year = race.get("year");
					$(this.el).append($('<div class="race-list-year">' + year +' </div>'));
				}
			
	    		race.set({ date: moment(new Date(race.get("year"), (race.get("month") - 1), race.get("day"))).format("MMM Do")});
            $(this.el).append(new RaceListItemView({model:race}).render().el);
        }, this);
        return this;
    }
});
 
window.RaceListItemView = Backbone.View.extend({
 
    tagName:"div",
 
    template:_.template($('#tpl-race-list-item').html()),
 
    render:function (eventName) {
		  $(this.el).addClass("race-list-item");
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }
});


window.RunnerResultsView = Backbone.View.extend({
 
	 tagName:"table", 

    template:_.template($('#tpl-runner-results-list').html()),

 	 initialize:function () {
        this.model.bind("reset", this.render, this);
    },
 
    render:function (eventName) {
			$(this.el).html(this.template());
         _.each(this.model.models, function (result) {
				//this is used in the sort	
				result.set({ date: new Date(result.get("race").year, (result.get("race").month - 1), result.get("race").day)});
				result.set({ "raceTime": Helper.formatRaceTime(result.get("hours"), result.get("minutes"), result.get("seconds"))});
				result.set({ "paceTime": Helper.formatRaceTime(0, result.get("paceminutes"), result.get("paceseconds"))});				
				$(this.el).append(new RunnerResultsItemView({model:result}).render().el);
        }, this);
        return this;
    }
});

window.RunnerResultsItemView = Backbone.View.extend({

	 tagName:"tr", 

    template:_.template($('#tpl-runner-results-list-item').html()),
 
    render:function (eventName) {
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }
});


window.RaceMainView = Backbone.View.extend({
  initialize: function (eventInfoView, raceResultsView) {
		$('#race-results').html(raceResultsView.render().el);
		$('#event-info').html(eventInfoView.render().el);
    	eventInfoView.on('yearChanged', raceResultsView.fetchRace, raceResultsView);
  }
});

window.EventInfoView = Backbone.View.extend({

  template:_.template($('#tpl-event-info').html()),

  initialize:function () {
    this.model.bind("reset", this.render, this);
  },

  events: {
    "change select": "onYearChanged"
  },

  setRaceDate : function(race){
    var date = moment(new Date(race.get("year"), (race.get("month") - 1), race.get("day"))).format("dddd, MMMM Do YYYY")
    $($(this.el).find("div#race-date")[0]).html(date);
  },

  onYearChanged: function(e){
    this.trigger('yearChanged', e.currentTarget.value);
    var selRace = this.model.where({race_id:  parseInt(e.currentTarget.value)});
    this.setRaceDate(selRace[0]);
  },
   
  render:function (eventName) {
    if (this.model.models.length > 0){
    $(this.el).html(this.template(this.model.models[0].toJSON()));
      var sel = $(this.el).find("select")[0];
      _.each(this.model.models, function (race) {	
        if (this.options.raceId == race.get("race_id")){
	  this.setRaceDate(race);
	  $(sel).append("<option value='" + race.get("race_id") + "' selected>" + race.get("year") + "</option>");
	}
	else{
	  $(sel).append("<option value='" + race.get("race_id") + "'>" + race.get("year") + "</option>");
	}
      }, this);
    }
    return this;
  }
});


window.RaceResultsView = Backbone.View.extend({
 
	 tagName: "div", 

    template:_.template($('#tpl-race-results-list').html()),

	 loadingtemplate: _.template($('#tpl-loading').html()),

 	 initialize:function () {
        this.model.bind("reset", this.render, this);
    },

    fetchRace: function (raceId) {
		this.showLoader();
      this.model.id = raceId;
      this.model.fetch();
    },
 
    render:function (eventName) {
			if (this.model.models.length === 0){
				this.showLoader();
			}
			else{
				$(this.el).html(this.template());
         	_.each(this.model.models, function (result) {	
					result.set({ "raceTime": Helper.formatRaceTime(result.get("hours"), result.get("minutes"), result.get("seconds"))});
					result.set({ "paceTime": Helper.formatRaceTime(0, result.get("paceminutes"), result.get("paceseconds"))});
					$($(this.el).children()[0]).append(new RaceResultsItemView({model:result}).render().el);
        		}, this);
			}
        	return this;
    },

	showLoader : function(){
		$(this.el).html(this.loadingtemplate());
	}
});

window.RaceResultsItemView = Backbone.View.extend({

	 tagName:"tr", 

    template:_.template($('#tpl-race-results-list-item').html()),
 
    render:function (eventName) {
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }
});



// Router
var AppRouter = Backbone.Router.extend({
 
    routes:{
        //"runners" : "runnerList",
			"" : "home",		  
			"races" : "raceList",
		  "results/:id" : "runnerResults",
		  "race/:eventid/:raceid" : "raceResults"
    },

		home: function(){
			displayView("home");
		},
 
    runnerList:function () {
        this.runnerList = new RunnerCollection();
        this.runnerListView = new RunnerListView({model:this.runnerList});
        this.runnerList.fetch();
        $('#runners').html(this.runnerListView.render().el);
    },

    raceList:function () {
				displayView("races");
        this.raceList = new RaceCollection();
        this.raceListView = new RaceListView({model:this.raceList});
        this.raceList.fetch();
        $('#race-list').html(this.raceListView.render().el);
    },

	runnerResults: function(id) {
		this.runnerResults = new RunnerResultsCollection([], {"id" : id});
		this.runnerResultsView = new RunnerResultsView({model:this.runnerResults});
		this.runnerResults.fetch();
		$('#runners').html(this.runnerResultsView.render().el);
  	},

	raceResults: function(eventid, raceid) {
		//list all the races for this event
		displayView("event");
		this.raceList = new RaceCollection([], {"id": eventid});
		this.eventInfoView = new EventInfoView({model:this.raceList, raceId: raceid});
		this.raceList.fetch();
		
		this.raceResults = new RaceResultsCollection([], {"id" : raceid});
		this.raceResultsView = new RaceResultsView({model:this.raceResults});
		this.raceResultsView.fetchRace(raceid);

		this.raceMainView = new RaceMainView(this.eventInfoView, this.raceResultsView)

  	}
 
});

window.displayView = function(view){
	var views = ["event", "races", "home"];

	var getViewName = function(v){
		return "#v-" + v;
	}

	_.each(views, function(v){
		$(getViewName(v)).hide();
	});

	$(getViewName(view)).show();
};
 
var app = new AppRouter();
Backbone.history.start();


