var Helper = {}

Helper.formatRaceTime = function(h, m, s){
	
	var sep = ":",
		 ret =  '',
		 hasTimeBeenFound = false,
		 el  = [h,m,s];

	_.each(el, function(el){
		if (el && el !== 0){
			hasTimeBeenFound = true;
		}

		if (hasTimeBeenFound){
			
			if (el.toString() == '0' || el.toString().length == 1){
				ret += '0' + el.toString();
			}
			else if (el){
				ret += el.toString();
			}
			ret += ':';
		}
	});

	return ret.substring(0, ret.length -1);
}
